<?php
/**
 * OfferTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\CatalogCacheClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Catalog Cache
 *
 * Ensi Catalog Cache
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\CatalogCacheClient;

use PHPUnit\Framework\TestCase;

/**
 * OfferTest Class Doc Comment
 *
 * @category    Class
 * @description Offer
 * @package     Ensi\CatalogCacheClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class OfferTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Offer"
     */
    public function testOffer()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "offer_id"
     */
    public function testPropertyOfferId()
    {
    }

    /**
     * Test attribute "product_id"
     */
    public function testPropertyProductId()
    {
    }

    /**
     * Test attribute "is_active"
     */
    public function testPropertyIsActive()
    {
    }

    /**
     * Test attribute "is_migrated"
     */
    public function testPropertyIsMigrated()
    {
    }

    /**
     * Test attribute "base_price"
     */
    public function testPropertyBasePrice()
    {
    }

    /**
     * Test attribute "price"
     */
    public function testPropertyPrice()
    {
    }

    /**
     * Test attribute "product"
     */
    public function testPropertyProduct()
    {
    }

    /**
     * Test attribute "discount"
     */
    public function testPropertyDiscount()
    {
    }

    /**
     * Test attribute "product_group"
     */
    public function testPropertyProductGroup()
    {
    }

    /**
     * Test attribute "product_group_product"
     */
    public function testPropertyProductGroupProduct()
    {
    }

    /**
     * Test attribute "created_at"
     */
    public function testPropertyCreatedAt()
    {
    }

    /**
     * Test attribute "updated_at"
     */
    public function testPropertyUpdatedAt()
    {
    }
}
