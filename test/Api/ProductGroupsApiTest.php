<?php
/**
 * ProductGroupsApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\CatalogCacheClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Catalog Cache
 *
 * Ensi Catalog Cache
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace Ensi\CatalogCacheClient;

use \Ensi\CatalogCacheClient\Configuration;
use \Ensi\CatalogCacheClient\ApiException;
use \Ensi\CatalogCacheClient\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * ProductGroupsApiTest Class Doc Comment
 *
 * @category Class
 * @package  Ensi\CatalogCacheClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class ProductGroupsApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for searchProductGroupProducts
     *
     * Search for objects of the type ProductGroupProducts.
     *
     */
    public function testSearchProductGroupProducts()
    {
    }

    /**
     * Test case for searchProductGroups
     *
     * Search for objects of the type ProductGroups.
     *
     */
    public function testSearchProductGroups()
    {
    }
}
