# Ensi\CatalogCacheClient\NameplatesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchNameplateProducts**](NameplatesApi.md#searchNameplateProducts) | **POST** /nameplates/nameplate-products:search | Search for objects of the type NameplateProduct
[**searchNameplates**](NameplatesApi.md#searchNameplates) | **POST** /nameplates/nameplates:search | Search for objects of the type Nameplate



## searchNameplateProducts

> \Ensi\CatalogCacheClient\Dto\SearchNameplateProductsResponse searchNameplateProducts($search_nameplate_products_request)

Search for objects of the type NameplateProduct

Search for objects of the type NameplateProduct

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\NameplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_nameplate_products_request = new \Ensi\CatalogCacheClient\Dto\SearchNameplateProductsRequest(); // \Ensi\CatalogCacheClient\Dto\SearchNameplateProductsRequest | 

try {
    $result = $apiInstance->searchNameplateProducts($search_nameplate_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NameplatesApi->searchNameplateProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_nameplate_products_request** | [**\Ensi\CatalogCacheClient\Dto\SearchNameplateProductsRequest**](../Model/SearchNameplateProductsRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchNameplateProductsResponse**](../Model/SearchNameplateProductsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchNameplates

> \Ensi\CatalogCacheClient\Dto\SearchNameplatesResponse searchNameplates($search_nameplates_request)

Search for objects of the type Nameplate

Search for objects of the type Nameplate

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\NameplatesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_nameplates_request = new \Ensi\CatalogCacheClient\Dto\SearchNameplatesRequest(); // \Ensi\CatalogCacheClient\Dto\SearchNameplatesRequest | 

try {
    $result = $apiInstance->searchNameplates($search_nameplates_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NameplatesApi->searchNameplates: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_nameplates_request** | [**\Ensi\CatalogCacheClient\Dto\SearchNameplatesRequest**](../Model/SearchNameplatesRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchNameplatesResponse**](../Model/SearchNameplatesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

