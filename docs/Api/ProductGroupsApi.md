# Ensi\CatalogCacheClient\ProductGroupsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchProductGroupProducts**](ProductGroupsApi.md#searchProductGroupProducts) | **POST** /products/product-group-products:search | Search for objects of the type ProductGroupProducts
[**searchProductGroups**](ProductGroupsApi.md#searchProductGroups) | **POST** /products/product-groups:search | Search for objects of the type ProductGroups



## searchProductGroupProducts

> \Ensi\CatalogCacheClient\Dto\SearchProductGroupProductsResponse searchProductGroupProducts($search_product_group_products_request)

Search for objects of the type ProductGroupProducts

Search for objects of the type ProductGroupProducts

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_group_products_request = new \Ensi\CatalogCacheClient\Dto\SearchProductGroupProductsRequest(); // \Ensi\CatalogCacheClient\Dto\SearchProductGroupProductsRequest | 

try {
    $result = $apiInstance->searchProductGroupProducts($search_product_group_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->searchProductGroupProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_group_products_request** | [**\Ensi\CatalogCacheClient\Dto\SearchProductGroupProductsRequest**](../Model/SearchProductGroupProductsRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchProductGroupProductsResponse**](../Model/SearchProductGroupProductsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductGroups

> \Ensi\CatalogCacheClient\Dto\SearchProductGroupsResponse searchProductGroups($search_product_groups_request)

Search for objects of the type ProductGroups

Search for objects of the type ProductGroups

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_groups_request = new \Ensi\CatalogCacheClient\Dto\SearchProductGroupsRequest(); // \Ensi\CatalogCacheClient\Dto\SearchProductGroupsRequest | 

try {
    $result = $apiInstance->searchProductGroups($search_product_groups_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->searchProductGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_groups_request** | [**\Ensi\CatalogCacheClient\Dto\SearchProductGroupsRequest**](../Model/SearchProductGroupsRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchProductGroupsResponse**](../Model/SearchProductGroupsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

