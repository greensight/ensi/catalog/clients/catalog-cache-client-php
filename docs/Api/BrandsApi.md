# Ensi\CatalogCacheClient\BrandsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchBrands**](BrandsApi.md#searchBrands) | **POST** /brands/brands:search | Search for objects of the type Brand



## searchBrands

> \Ensi\CatalogCacheClient\Dto\SearchBrandsResponse searchBrands($search_brands_request)

Search for objects of the type Brand

Search for objects of the type Brand

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\BrandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_brands_request = new \Ensi\CatalogCacheClient\Dto\SearchBrandsRequest(); // \Ensi\CatalogCacheClient\Dto\SearchBrandsRequest | 

try {
    $result = $apiInstance->searchBrands($search_brands_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BrandsApi->searchBrands: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_brands_request** | [**\Ensi\CatalogCacheClient\Dto\SearchBrandsRequest**](../Model/SearchBrandsRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchBrandsResponse**](../Model/SearchBrandsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

