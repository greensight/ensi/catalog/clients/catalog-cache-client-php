# Ensi\CatalogCacheClient\ElasticApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteIndexerTimestamp**](ElasticApi.md#deleteIndexerTimestamp) | **DELETE** /elastic/indexer-timestamps/{id} | Deleting an object of IndexerTimestamp
[**patchIndexerTimestamp**](ElasticApi.md#patchIndexerTimestamp) | **PATCH** /elastic/indexer-timestamps/{id} | Patching an object of IndexerTimestamp
[**searchIndexerTimestamps**](ElasticApi.md#searchIndexerTimestamps) | **POST** /elastic/indexer-timestamps:search | Search for objects of IndexerTimestamp



## deleteIndexerTimestamp

> \Ensi\CatalogCacheClient\Dto\EmptyDataResponse deleteIndexerTimestamp($id)

Deleting an object of IndexerTimestamp

Deleting an object of IndexerTimestamp

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\ElasticApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID

try {
    $result = $apiInstance->deleteIndexerTimestamp($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ElasticApi->deleteIndexerTimestamp: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |

### Return type

[**\Ensi\CatalogCacheClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchIndexerTimestamp

> \Ensi\CatalogCacheClient\Dto\IndexerTimestampResponse patchIndexerTimestamp($id, $patch_indexer_timestamp_request)

Patching an object of IndexerTimestamp

Patching an object of IndexerTimestamp

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\ElasticApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric ID
$patch_indexer_timestamp_request = new \Ensi\CatalogCacheClient\Dto\PatchIndexerTimestampRequest(); // \Ensi\CatalogCacheClient\Dto\PatchIndexerTimestampRequest | 

try {
    $result = $apiInstance->patchIndexerTimestamp($id, $patch_indexer_timestamp_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ElasticApi->patchIndexerTimestamp: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric ID |
 **patch_indexer_timestamp_request** | [**\Ensi\CatalogCacheClient\Dto\PatchIndexerTimestampRequest**](../Model/PatchIndexerTimestampRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\IndexerTimestampResponse**](../Model/IndexerTimestampResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchIndexerTimestamps

> \Ensi\CatalogCacheClient\Dto\SearchIndexerTimestampsResponse searchIndexerTimestamps($search_indexer_timestamps_request)

Search for objects of IndexerTimestamp

Search for objects of IndexerTimestamp

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\ElasticApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_indexer_timestamps_request = new \Ensi\CatalogCacheClient\Dto\SearchIndexerTimestampsRequest(); // \Ensi\CatalogCacheClient\Dto\SearchIndexerTimestampsRequest | 

try {
    $result = $apiInstance->searchIndexerTimestamps($search_indexer_timestamps_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ElasticApi->searchIndexerTimestamps: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_indexer_timestamps_request** | [**\Ensi\CatalogCacheClient\Dto\SearchIndexerTimestampsRequest**](../Model/SearchIndexerTimestampsRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchIndexerTimestampsResponse**](../Model/SearchIndexerTimestampsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

