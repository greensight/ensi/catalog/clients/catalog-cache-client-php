# Ensi\CatalogCacheClient\ProductsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchImages**](ProductsApi.md#searchImages) | **POST** /products/images:search | Search for objects of Image
[**searchProductPropertyValues**](ProductsApi.md#searchProductPropertyValues) | **POST** /products/product-property-values:search | Search for objects of ProductPropertyValue
[**searchProducts**](ProductsApi.md#searchProducts) | **POST** /products/products:search | Search for objects of Product



## searchImages

> \Ensi\CatalogCacheClient\Dto\SearchImagesResponse searchImages($search_images_request)

Search for objects of Image

Search for objects of Image

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_images_request = new \Ensi\CatalogCacheClient\Dto\SearchImagesRequest(); // \Ensi\CatalogCacheClient\Dto\SearchImagesRequest | 

try {
    $result = $apiInstance->searchImages($search_images_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->searchImages: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_images_request** | [**\Ensi\CatalogCacheClient\Dto\SearchImagesRequest**](../Model/SearchImagesRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchImagesResponse**](../Model/SearchImagesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductPropertyValues

> \Ensi\CatalogCacheClient\Dto\SearchProductPropertyValuesResponse searchProductPropertyValues($search_product_property_values_request)

Search for objects of ProductPropertyValue

Search for objects of ProductPropertyValue

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_property_values_request = new \Ensi\CatalogCacheClient\Dto\SearchProductPropertyValuesRequest(); // \Ensi\CatalogCacheClient\Dto\SearchProductPropertyValuesRequest | 

try {
    $result = $apiInstance->searchProductPropertyValues($search_product_property_values_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->searchProductPropertyValues: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_property_values_request** | [**\Ensi\CatalogCacheClient\Dto\SearchProductPropertyValuesRequest**](../Model/SearchProductPropertyValuesRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchProductPropertyValuesResponse**](../Model/SearchProductPropertyValuesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProducts

> \Ensi\CatalogCacheClient\Dto\SearchProductsResponse searchProducts($search_products_request)

Search for objects of Product

Search for objects of Product

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_products_request = new \Ensi\CatalogCacheClient\Dto\SearchProductsRequest(); // \Ensi\CatalogCacheClient\Dto\SearchProductsRequest | 

try {
    $result = $apiInstance->searchProducts($search_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->searchProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_products_request** | [**\Ensi\CatalogCacheClient\Dto\SearchProductsRequest**](../Model/SearchProductsRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchProductsResponse**](../Model/SearchProductsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

