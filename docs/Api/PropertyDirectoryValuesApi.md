# Ensi\CatalogCacheClient\PropertyDirectoryValuesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchPropertyDirectoryValues**](PropertyDirectoryValuesApi.md#searchPropertyDirectoryValues) | **POST** /properties/property-directory-values:search | Search for objects of the type PropertyDirectoryValues



## searchPropertyDirectoryValues

> \Ensi\CatalogCacheClient\Dto\SearchPropertyDirectoryValuesResponse searchPropertyDirectoryValues($search_property_directory_values_request)

Search for objects of the type PropertyDirectoryValues

Search for objects of the type PropertyDirectoryValues

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\PropertyDirectoryValuesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_property_directory_values_request = new \Ensi\CatalogCacheClient\Dto\SearchPropertyDirectoryValuesRequest(); // \Ensi\CatalogCacheClient\Dto\SearchPropertyDirectoryValuesRequest | 

try {
    $result = $apiInstance->searchPropertyDirectoryValues($search_property_directory_values_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertyDirectoryValuesApi->searchPropertyDirectoryValues: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_property_directory_values_request** | [**\Ensi\CatalogCacheClient\Dto\SearchPropertyDirectoryValuesRequest**](../Model/SearchPropertyDirectoryValuesRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchPropertyDirectoryValuesResponse**](../Model/SearchPropertyDirectoryValuesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

