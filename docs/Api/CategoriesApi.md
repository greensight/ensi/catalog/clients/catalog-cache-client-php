# Ensi\CatalogCacheClient\CategoriesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchActualCategoryProperties**](CategoriesApi.md#searchActualCategoryProperties) | **POST** /categories/actual-properties:search | Search for objects of the type ActualCategoryProperties
[**searchCategories**](CategoriesApi.md#searchCategories) | **POST** /categories/categories:search | Search for objects of the type Category



## searchActualCategoryProperties

> \Ensi\CatalogCacheClient\Dto\SearchActualCategoryPropertiesResponse searchActualCategoryProperties($search_actual_category_properties_request)

Search for objects of the type ActualCategoryProperties

Search for objects of the type ActualCategoryProperties

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_actual_category_properties_request = new \Ensi\CatalogCacheClient\Dto\SearchActualCategoryPropertiesRequest(); // \Ensi\CatalogCacheClient\Dto\SearchActualCategoryPropertiesRequest | 

try {
    $result = $apiInstance->searchActualCategoryProperties($search_actual_category_properties_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->searchActualCategoryProperties: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_actual_category_properties_request** | [**\Ensi\CatalogCacheClient\Dto\SearchActualCategoryPropertiesRequest**](../Model/SearchActualCategoryPropertiesRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchActualCategoryPropertiesResponse**](../Model/SearchActualCategoryPropertiesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchCategories

> \Ensi\CatalogCacheClient\Dto\SearchCategoriesResponse searchCategories($search_categories_request)

Search for objects of the type Category

Search for objects of the type Category

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_categories_request = new \Ensi\CatalogCacheClient\Dto\SearchCategoriesRequest(); // \Ensi\CatalogCacheClient\Dto\SearchCategoriesRequest | 

try {
    $result = $apiInstance->searchCategories($search_categories_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->searchCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_categories_request** | [**\Ensi\CatalogCacheClient\Dto\SearchCategoriesRequest**](../Model/SearchCategoriesRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchCategoriesResponse**](../Model/SearchCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

