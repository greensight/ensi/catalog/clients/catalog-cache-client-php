# Ensi\CatalogCacheClient\ElasticOffersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getElasticOffer**](ElasticOffersApi.md#getElasticOffer) | **GET** /elastic-offers/offers/{id} | Get the object of Offer
[**searchElasticOffers**](ElasticOffersApi.md#searchElasticOffers) | **POST** /elastic-offers/offers:search | Search for objects of Offers
[**searchOneElasticOffer**](ElasticOffersApi.md#searchOneElasticOffer) | **POST** /elastic-offers/offers:search-one | Search for object of Offers



## getElasticOffer

> \Ensi\CatalogCacheClient\Dto\ElasticOfferResponse getElasticOffer($id, $include)

Get the object of Offer

Get the object of Offer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\ElasticOffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id
$include = 'include_example'; // string | Related entities for uploading, separated by commas

try {
    $result = $apiInstance->getElasticOffer($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ElasticOffersApi->getElasticOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |
 **include** | **string**| Related entities for uploading, separated by commas | [optional]

### Return type

[**\Ensi\CatalogCacheClient\Dto\ElasticOfferResponse**](../Model/ElasticOfferResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchElasticOffers

> \Ensi\CatalogCacheClient\Dto\SearchElasticOffersResponse searchElasticOffers($search_elastic_offers_request)

Search for objects of Offers

Search for objects of Offers

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\ElasticOffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_elastic_offers_request = new \Ensi\CatalogCacheClient\Dto\SearchElasticOffersRequest(); // \Ensi\CatalogCacheClient\Dto\SearchElasticOffersRequest | 

try {
    $result = $apiInstance->searchElasticOffers($search_elastic_offers_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ElasticOffersApi->searchElasticOffers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_elastic_offers_request** | [**\Ensi\CatalogCacheClient\Dto\SearchElasticOffersRequest**](../Model/SearchElasticOffersRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchElasticOffersResponse**](../Model/SearchElasticOffersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneElasticOffer

> \Ensi\CatalogCacheClient\Dto\ElasticOfferResponse searchOneElasticOffer($search_one_elastic_offers_request)

Search for object of Offers

Search for object of Offers

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\ElasticOffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_one_elastic_offers_request = new \Ensi\CatalogCacheClient\Dto\SearchOneElasticOffersRequest(); // \Ensi\CatalogCacheClient\Dto\SearchOneElasticOffersRequest | 

try {
    $result = $apiInstance->searchOneElasticOffer($search_one_elastic_offers_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ElasticOffersApi->searchOneElasticOffer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_one_elastic_offers_request** | [**\Ensi\CatalogCacheClient\Dto\SearchOneElasticOffersRequest**](../Model/SearchOneElasticOffersRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\ElasticOfferResponse**](../Model/ElasticOfferResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

