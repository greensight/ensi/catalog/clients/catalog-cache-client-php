# Ensi\CatalogCacheClient\DiscountsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchDiscounts**](DiscountsApi.md#searchDiscounts) | **POST** /discounts/discounts:search | Search for objects of Discount



## searchDiscounts

> \Ensi\CatalogCacheClient\Dto\SearchDiscountsResponse searchDiscounts($search_discounts_request)

Search for objects of Discount

Search for objects of Discount

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_discounts_request = new \Ensi\CatalogCacheClient\Dto\SearchDiscountsRequest(); // \Ensi\CatalogCacheClient\Dto\SearchDiscountsRequest | 

try {
    $result = $apiInstance->searchDiscounts($search_discounts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->searchDiscounts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_discounts_request** | [**\Ensi\CatalogCacheClient\Dto\SearchDiscountsRequest**](../Model/SearchDiscountsRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchDiscountsResponse**](../Model/SearchDiscountsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

