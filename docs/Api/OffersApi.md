# Ensi\CatalogCacheClient\OffersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchOffers**](OffersApi.md#searchOffers) | **POST** /offers/offers:search | Search for objects of Offer



## searchOffers

> \Ensi\CatalogCacheClient\Dto\SearchOffersResponse searchOffers($search_offers_request)

Search for objects of Offer

Search for objects of Offer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\OffersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_offers_request = new \Ensi\CatalogCacheClient\Dto\SearchOffersRequest(); // \Ensi\CatalogCacheClient\Dto\SearchOffersRequest | 

try {
    $result = $apiInstance->searchOffers($search_offers_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OffersApi->searchOffers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_offers_request** | [**\Ensi\CatalogCacheClient\Dto\SearchOffersRequest**](../Model/SearchOffersRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchOffersResponse**](../Model/SearchOffersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

