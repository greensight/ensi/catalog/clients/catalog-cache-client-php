# Ensi\CatalogCacheClient\PropertiesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchProperties**](PropertiesApi.md#searchProperties) | **POST** /properties/properties:search | Search for objects of the type Property



## searchProperties

> \Ensi\CatalogCacheClient\Dto\SearchPropertiesResponse searchProperties($search_properties_request)

Search for objects of the type Property

Search for objects of the type Property

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CatalogCacheClient\Api\PropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_properties_request = new \Ensi\CatalogCacheClient\Dto\SearchPropertiesRequest(); // \Ensi\CatalogCacheClient\Dto\SearchPropertiesRequest | 

try {
    $result = $apiInstance->searchProperties($search_properties_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PropertiesApi->searchProperties: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_properties_request** | [**\Ensi\CatalogCacheClient\Dto\SearchPropertiesRequest**](../Model/SearchPropertiesRequest.md)|  |

### Return type

[**\Ensi\CatalogCacheClient\Dto\SearchPropertiesResponse**](../Model/SearchPropertiesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

