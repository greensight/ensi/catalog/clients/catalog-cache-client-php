# # ElasticBrand

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID бренда из PIM | 
**name** | **string** | Название | 
**code** | **string** | ЧПУ код | 
**description** | **string** | Описание бренда | 
**logo_file** | [**\Ensi\CatalogCacheClient\Dto\File**](File.md) |  | 
**logo_url** | **string** | URL логотипа | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


