# # ProductPropertyValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Product property value ID | 
**product_property_value_id** | **int** | Product property value ID from PIM | 
**product_id** | **int** | Product ID from PIM | 
**property_id** | **int** | Property ID from PIM | 
**directory_value_id** | **int** | Directory value ID from PIM | [optional] 
**type** | **string** | Value type | 
**value** | **string** | Value | 
**name** | **string** | Value name | 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**property** | [**\Ensi\CatalogCacheClient\Dto\Property**](Property.md) |  | [optional] 
**directory_value** | [**\Ensi\CatalogCacheClient\Dto\PropertyDirectoryValue**](PropertyDirectoryValue.md) |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Product property created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Product property updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


