# # ElasticDiscount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value_type** | **int** | Тип значения скидки из DiscountValueTypeEnum | 
**value** | **int** | Размер скидки | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


