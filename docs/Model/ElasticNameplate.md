# # ElasticNameplate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | 
**name** | **string** | Наименование тега | 
**code** | **string** | Код тега | 
**background_color** | **string** | Цвет фона | 
**text_color** | **string** | Цвет текста | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


