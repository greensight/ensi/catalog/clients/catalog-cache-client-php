# # PropertyDirectoryValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Directory value id | 
**directory_value_id** | **int** | Directory value id from PIM | 
**property_id** | **int** | Property ID from PIM | 
**name** | **string** | Value name | 
**code** | **string** | Code | 
**value** | **string** | Value | 
**type** | **string** | Type of stored value | 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**created_at** | [**\DateTime**](\DateTime.md) | Nameplate created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Nameplate updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


