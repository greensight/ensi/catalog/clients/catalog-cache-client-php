# # Offer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Offer ID | 
**offer_id** | **int** | Offer ID from Offers | 
**product_id** | **int** | Product ID from Pim | 
**is_active** | **bool** | The final activity of the offer | 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**base_price** | **int** | Base price | [optional] 
**price** | **int** | The final discounted price. If null, it is calculated during indexing | [optional] 
**product** | [**\Ensi\CatalogCacheClient\Dto\Product**](Product.md) |  | [optional] 
**discount** | [**\Ensi\CatalogCacheClient\Dto\Discount**](Discount.md) |  | [optional] 
**product_group** | [**\Ensi\CatalogCacheClient\Dto\ProductGroup**](ProductGroup.md) |  | [optional] 
**product_group_product** | [**\Ensi\CatalogCacheClient\Dto\ProductGroupProduct**](ProductGroupProduct.md) |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Offer created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Offer updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


