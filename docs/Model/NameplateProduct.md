# # NameplateProduct

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | NameplateProduct ID | 
**nameplate_product_id** | **int** | Bundles of nameplate and product from CMS | 
**nameplate_id** | **int** | Nameplate ID from CMS | 
**product_id** | **int** | Product ID from PIM | 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**created_at** | [**\DateTime**](\DateTime.md) | NameplateProduct created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | NameplateProduct updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


