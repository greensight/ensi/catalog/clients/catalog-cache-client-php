# # Brand

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Brand ID | 
**brand_id** | **int** | Brand ID from PIM | 
**name** | **string** | Name | 
**code** | **string** | CNC code | 
**description** | **string** | Brand description | [optional] 
**logo_file** | **string** | Logotype file | [optional] 
**logo_url** | **string** | Logotype URL | [optional] 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**created_at** | [**\DateTime**](\DateTime.md) | Brand created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Brand updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


