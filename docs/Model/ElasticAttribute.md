# # ElasticAttribute

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Тип | 
**name** | **string** | Название | 
**code** | **string** | Код атрибута | 
**values** | [**\Ensi\CatalogCacheClient\Dto\AttributeValue[]**](AttributeValue.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


