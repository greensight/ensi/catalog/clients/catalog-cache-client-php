# # Image

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Image ID | 
**image_id** | **int** | Image ID from PIM | 
**product_id** | **int** | Product ID from PIM | 
**name** | **string** | Image description | 
**sort** | **int** | Sorting order | 
**file** | **string** | Image file | 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**created_at** | [**\DateTime**](\DateTime.md) | Image created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Image updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


