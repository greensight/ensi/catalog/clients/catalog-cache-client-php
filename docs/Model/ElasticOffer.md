# # ElasticOffer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Offer ID from Offers | 
**product_id** | **int** | Product ID from PIM | 
**allow_publish** | **bool** |  | 
**main_image_file** | [**\Ensi\CatalogCacheClient\Dto\FileNullable**](FileNullable.md) |  | 
**category_ids** | **int[]** | Category IDs from PIM | 
**brand_id** | **int** | Brand ID from PIM | 
**name** | **string** |  | 
**code** | **string** | Code to use in URL | 
**description** | **string** |  | 
**type** | **int** | Product type from ProductTypeEnum from PIM | 
**vendor_code** | **string** | Article | 
**barcode** | **string** | EAN | 
**weight** | **float** | Net weight in kg | 
**weight_gross** | **float** | Gross weight in kg | 
**width** | **float** | Width in mm | 
**height** | **float** | Height in mm | 
**length** | **float** | Length in mm | 
**is_adult** | **bool** | Is product 18+ | 
**uom** | **int** | Unit of measurement from ProductUomEnum from PIM | 
**tariffing_volume** | **int** | Unit of tariffication from ProductTariffingVolumeEnum from PIM | 
**order_step** | **float** |  | 
**order_minvol** | **float** | Minimum quantity for order | 
**price** | **int** | Final price | 
**cost** | **int** | Cost before discount | 
**gluing_name** | **string** | Product group name | 
**gluing_is_main** | **bool** | Is main product of product group | 
**gluing_is_active** | **bool** | Is product group active | 
**discount** | [**\Ensi\CatalogCacheClient\Dto\ElasticDiscount**](ElasticDiscount.md) |  | [optional] 
**brand** | [**\Ensi\CatalogCacheClient\Dto\ElasticBrand**](ElasticBrand.md) |  | [optional] 
**categories** | [**\Ensi\CatalogCacheClient\Dto\ElasticCategory[]**](ElasticCategory.md) |  | [optional] 
**images** | [**\Ensi\CatalogCacheClient\Dto\ElasticImage[]**](ElasticImage.md) |  | [optional] 
**attributes** | [**\Ensi\CatalogCacheClient\Dto\ElasticAttribute[]**](ElasticAttribute.md) |  | [optional] 
**gluing** | [**\Ensi\CatalogCacheClient\Dto\Gluing[]**](Gluing.md) |  | [optional] 
**nameplates** | [**\Ensi\CatalogCacheClient\Dto\ElasticNameplate[]**](ElasticNameplate.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


