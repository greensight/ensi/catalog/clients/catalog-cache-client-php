# # Nameplate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Nameplate ID from CMS | 
**nameplate_id** | **int** | Nameplate ID from CMS | 
**name** | **string** | Nameplate name | 
**code** | **string** | Nameplate code | 
**background_color** | **string** | Background color | 
**text_color** | **string** | Text color | 
**is_active** | **bool** | Active | 
**product_links** | [**\Ensi\CatalogCacheClient\Dto\NameplateProduct[]**](NameplateProduct.md) | Array of related NameplateProduct objects | [optional] 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**created_at** | [**\DateTime**](\DateTime.md) | Nameplate created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Nameplate updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


