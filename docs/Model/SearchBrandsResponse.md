# # SearchBrandsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\CatalogCacheClient\Dto\Brand[]**](Brand.md) |  | 
**meta** | [**\Ensi\CatalogCacheClient\Dto\SearchFailedJobsResponseMeta**](SearchFailedJobsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


