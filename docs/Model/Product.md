# # Product

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID | 
**product_id** | **int** | Product ID from PIM | 
**allow_publish** | **bool** |  | 
**main_image_file** | **string** | Main image file | 
**category_ids** | **int[]** | Category IDs from PIM | 
**brand_id** | **int** | Brand ID from PIM | 
**name** | **string** |  | 
**code** | **string** | Code to use in URL | 
**description** | **string** |  | 
**type** | **int** | Product type from ProductTypeEnum from PIM | 
**vendor_code** | **string** | Article | 
**barcode** | **string** | EAN | 
**weight** | **float** | Net weight in kg | 
**weight_gross** | **float** | Gross weight in kg | 
**width** | **float** | Width in mm | 
**height** | **float** | Height in mm | 
**length** | **float** | Length in mm | 
**is_adult** | **bool** | Is product 18+ | 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**uom** | **int** | Unit of measurement from ProductUomEnum from PIM | 
**tariffing_volume** | **int** | Unit of tariffication from ProductTariffingVolumeEnum from PIM | 
**order_step** | **float** |  | 
**order_minvol** | **float** |  | 
**brand** | [**\Ensi\CatalogCacheClient\Dto\Brand**](Brand.md) |  | [optional] 
**categories** | [**\Ensi\CatalogCacheClient\Dto\Category[]**](Category.md) |  | [optional] 
**images** | [**\Ensi\CatalogCacheClient\Dto\Image[]**](Image.md) |  | [optional] 
**product_property_values** | [**\Ensi\CatalogCacheClient\Dto\ProductPropertyValue[]**](ProductPropertyValue.md) |  | [optional] 
**nameplates** | [**\Ensi\CatalogCacheClient\Dto\Nameplate[]**](Nameplate.md) | Product nameplates | [optional] 
**product_group** | [**\Ensi\CatalogCacheClient\Dto\ProductGroup**](ProductGroup.md) |  | [optional] 
**offers** | [**\Ensi\CatalogCacheClient\Dto\Offer[]**](Offer.md) | Product offers | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Product created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Product updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


