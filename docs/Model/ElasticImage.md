# # ElasticImage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID изображения из PIM | 
**name** | **string** | Название | 
**sort** | **int** | Порядок сортировки | 
**url** | **string** | URL файла | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


