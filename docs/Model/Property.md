# # Property

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Category ID from Pim | 
**property_id** | **int** | Property ID from PIM | 
**name** | **string** | Name | 
**code** | **string** | Property code | 
**type** | **string** | Type | 
**is_public** | **bool** | Display on the showcase | 
**is_active** | **bool** | Property active | 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**created_at** | [**\DateTime**](\DateTime.md) | Property created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Property updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


