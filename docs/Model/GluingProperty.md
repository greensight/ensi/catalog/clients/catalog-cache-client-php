# # GluingProperty

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prop_type** | **string** | Тип атрибута | 
**prop_name** | **string** | Название атрибута на витрине | 
**prop_code** | **string** | Код атрибута | 
**value_value** | **string** | Значение атрибута | 
**value_name** | **string** | Название значения атрибута | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


