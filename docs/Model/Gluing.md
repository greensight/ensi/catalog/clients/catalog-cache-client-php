# # Gluing

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID товара | 
**name** | **string** | Название товара | 
**code** | **string** | ЧПУ код товара | 
**props** | [**\Ensi\CatalogCacheClient\Dto\GluingProperty[]**](GluingProperty.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


