# # IndexerTimestampReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**index** | **string** | The base name of the index (products, categories) | 
**stage** | **string** | The branch where indexing takes place | 
**index_hash** | **string** | Index hash | 
**is_current_index** | **bool** | This setting is for the current hash | 
**is_current_stage** | **bool** | This setting is for the current APP_STAGE | 
**is_current** | **bool** | This setting is for current settings | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


