# # IndexerTimestampFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_schedule** | [**\DateTime**](\DateTime.md) | Last schedule time | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


