# # ActualCategoryProperty

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID of actual category property | 
**actual_category_property_id** | **int** | ID of actual category property from PIM | 
**property_id** | **int** | Property ID from PIM | 
**category_id** | **int** | Category ID from PIM | 
**is_gluing** | **bool** | The property is used for gluing products | 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**created_at** | [**\DateTime**](\DateTime.md) | Property created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Property updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


