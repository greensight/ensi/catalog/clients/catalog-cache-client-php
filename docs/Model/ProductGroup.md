# # ProductGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Product gluing ID | 
**product_group_id** | **int** | Product gluing ID from PIM | 
**category_id** | **int** | Category ID from PIM | 
**name** | **string** | Name | 
**main_product_id** | **int** | Main product ID from PIM | 
**is_active** | **bool** | Activity from PIM | 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**category** | [**\Ensi\CatalogCacheClient\Dto\Category**](Category.md) |  | [optional] 
**main_product** | [**\Ensi\CatalogCacheClient\Dto\Product**](Product.md) |  | [optional] 
**products** | [**\Ensi\CatalogCacheClient\Dto\Product[]**](Product.md) | Products gluing | [optional] 
**product_links** | [**\Ensi\CatalogCacheClient\Dto\ProductGroupProduct[]**](ProductGroupProduct.md) | Product links to gluing | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Product Group created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Product Group updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


