# # Category

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Category ID | 
**name** | **string** | Name | 
**code** | **string** | CNC code | 
**parent_id** | **int** | ID of the parent category from PIM | 
**category_id** | **int** | Category ID from PIM | 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**actual_properties** | [**\Ensi\CatalogCacheClient\Dto\ActualCategoryProperty[]**](ActualCategoryProperty.md) | Array of related ActualCategoryProperty objects | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Property created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Property updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


