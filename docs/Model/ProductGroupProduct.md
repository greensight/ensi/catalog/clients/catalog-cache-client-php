# # ProductGroupProduct

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID of the product link and the product | 
**product_group_product_id** | **int** | ID of the product link and the product from PIM | 
**product_group_id** | **int** | Product gluing ID from PIM | 
**product_id** | **int** | Product ID from PIM | 
**product_group** | [**\Ensi\CatalogCacheClient\Dto\ProductGroup**](ProductGroup.md) |  | [optional] 
**product** | [**\Ensi\CatalogCacheClient\Dto\Product**](Product.md) |  | [optional] 
**is_migrated** | **bool** | Saved/created during migration of records from master services | 
**created_at** | [**\DateTime**](\DateTime.md) | Product Group Product created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Product Group Product updated at | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


