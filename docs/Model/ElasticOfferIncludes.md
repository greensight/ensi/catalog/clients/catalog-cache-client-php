# # ElasticOfferIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discount** | [**\Ensi\CatalogCacheClient\Dto\ElasticDiscount**](ElasticDiscount.md) |  | [optional] 
**brand** | [**\Ensi\CatalogCacheClient\Dto\ElasticBrand**](ElasticBrand.md) |  | [optional] 
**categories** | [**\Ensi\CatalogCacheClient\Dto\ElasticCategory[]**](ElasticCategory.md) |  | [optional] 
**images** | [**\Ensi\CatalogCacheClient\Dto\ElasticImage[]**](ElasticImage.md) |  | [optional] 
**attributes** | [**\Ensi\CatalogCacheClient\Dto\ElasticAttribute[]**](ElasticAttribute.md) |  | [optional] 
**gluing** | [**\Ensi\CatalogCacheClient\Dto\Gluing[]**](Gluing.md) |  | [optional] 
**nameplates** | [**\Ensi\CatalogCacheClient\Dto\ElasticNameplate[]**](ElasticNameplate.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


