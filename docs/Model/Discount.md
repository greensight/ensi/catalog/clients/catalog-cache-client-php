# # Discount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID | 
**offer_id** | **int** | Offer ID from Offers | 
**value_type** | **int** | Value type | 
**value** | **int** | Discount value | 
**created_at** | [**\DateTime**](\DateTime.md) | Discount created at | 
**updated_at** | [**\DateTime**](\DateTime.md) | Discount updated at | 
**offer** | [**\Ensi\CatalogCacheClient\Dto\Offer**](Offer.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


