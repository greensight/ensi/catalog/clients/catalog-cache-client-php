# # ElasticCategory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Category ID from PIM | 
**name** | **string** |  | 
**code** | **string** | Code to use in URL | 
**parent_id** | **int** | Parent category ID from PIM | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


